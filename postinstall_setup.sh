# remove annoying sound ... tab-beeps from terminal
# remove scrollbar from terminal



# Only alt-tab within current workspace only
gsettings set org.gnome.shell.app-switcher current-workspace-only 'true'

# Enable Fractional scaling
gsettings set org.gnome.mutter experimental-features "['scale-monitor-framebuffer']"

# Dark mode 
gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"

# New windows should be centered 
gsettings set org.gnome.mutter center-new-windows 'true'
 
# Disable dynamic workspaces and set a fixed 6
gsettings set org.gnome.mutter dynamic-workspaces 'false'
gsettings set org.gnome.desktop.wm.preferences num-workspaces 6

# Remove these keybindings. cus' they f-ing sux
gsettings set org.gnome.shell.keybindings switch-to-application-1 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-2 "[]" 
gsettings set org.gnome.shell.keybindings switch-to-application-3 "[]" 
gsettings set org.gnome.shell.keybindings switch-to-application-4 "[]" 
gsettings set org.gnome.shell.keybindings switch-to-application-5 "[]" 
gsettings set org.gnome.shell.keybindings switch-to-application-6 "[]" 
gsettings set org.gnome.shell.keybindings switch-to-application-7 "[]" 
gsettings set org.gnome.shell.keybindings switch-to-application-8 "[]" 
gsettings set org.gnome.shell.keybindings switch-to-application-9 "[]"

# Keybindings
gsettings set org.gnome.desktop.wm.keybindings cycle-group "['<Super>section']"
gsettings set org.gnome.desktop.wm.keybindings cycle-group-backward "['<Shift><Super>section']"
gsettings set org.gnome.desktop.wm.keybindings cycle-windows "['<Super>Tab']"
gsettings set org.gnome.desktop.wm.keybindings cycle-windows-backward "['<Shift><Super>Tab']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-1 "['<Shift><Super>exclam']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-2 "['<Shift><Super>quotedbl']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-3 "['<Shift><Super>numbersign']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-4 "['<Shift><Super>currency']"
gsettings set org.gnome.desktop.wm.keybindings switch-applications "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-applications-backward "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-group "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-group-backward "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Super>1']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Super>2']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Super>3']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Super>4']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-5 "['<Super>5']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-6 "['<Super>6']"
gsettings set org.gnome.desktop.wm.keybindings toggle-fullscreen "['<Super>f']"

# Change terminal app

# get name of current profile
export GNOME_TERMINAL_PROFILE=`gsettings get org.gnome.Terminal.ProfilesList default | awk -F \' '{print $2}'`

#gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ font 'Monospace 10'
#gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ use-system-font false
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ audible-bell false
#gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ use-theme-colors false
#gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ background-color '#000000'
#gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ foreground-color '#AFAFAF'
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ scrollbar-policy 'never'

# DISABLE RETARDED GNOME TERMINAL BEHAVIOUR
# manual step
# add ' printf "\033]777;container;pop;;\033\\" ' to /etc/profile.d/99-disable-automatic-toolbox.sh

